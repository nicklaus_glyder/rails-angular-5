// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// JQUERY AND RAILS INTEGRATION
//= require jquery
//= require jquery_ujs
// ANGULAR COMPONENTS
//= require angular
//= require angular-ui-router
// ANGULAR MATERIAL LIBRARIES
//= require angular-material
//= require angular-animate
//= require angular-aria
//= require angular-rails-templates
// APP MODULES
//= require app.module
// EVERYTHING ELSE - Note that template engine works here
//= require_tree .
