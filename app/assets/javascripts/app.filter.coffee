angular.module('test-site')
  .filter('dotToDash', () ->
    return (string) ->
      return string.split('.').join('-');
  );
