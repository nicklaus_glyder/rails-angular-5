Rails + Angular + Material Template App
===
This is a Rails 5 app, built for interfacing with an Angular 1.5+ application.
Also has Angular Material support.